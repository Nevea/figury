package figury;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.image.MemoryImageSource;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.Timer;

public class AnimPanel extends JPanel implements ActionListener, ComponentListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// bufor
	Image image;
	// wykreslacz ekranowy
	Graphics2D device;
	// wykreslacz bufora
	Graphics2D buffer;
	private List<Figura> fig = new ArrayList<Figura>();
	private List<Thread> threads = new ArrayList<Thread>();
	
	private int delay = 17;

	private Timer timer;

	private static int numer = 0;

	public AnimPanel() {
		super();
		setBackground(Color.WHITE);
		timer = new Timer(delay, this);
	}

	public void initialize() {
		int width = getWidth();
		int height = getHeight();

		image = createImage(width, height);
		buffer = (Graphics2D) image.getGraphics();
		buffer.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		device = (Graphics2D) getGraphics();
		device.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		this.addComponentListener(this);
	}

	void addFig() {
		numer++;
		if(numer%3 == 1)
			fig.add(new Kwadrat(buffer, delay, getWidth(), getHeight()));
		if(numer%3 == 2)
			fig.add(new Elipsa(buffer, delay, getWidth(), getHeight()));
		if(numer%3 == 0)
			fig.add(new Trojkat(buffer, delay, getWidth(), getHeight()));
		System.out.println(numer%3);
		timer.addActionListener(fig.get(numer-1));
		threads.add(new Thread(fig.get(numer-1)));
		threads.get(threads.size()-1).start();
	}

	@SuppressWarnings("deprecation")
	void animate() {
		if (timer.isRunning()) {
			timer.stop();
			for(int i = 0; i < numer; i++)
			{
				threads.get(i).suspend();
			}
		} else {
			timer.start();
			for(int i = 0; i < numer; i++)
			{
				threads.get(i).resume();
			}
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
			device.drawImage(image, 0, 0, null);
			buffer.clearRect(0, 0, getWidth(), getHeight());
	}
	
	@Override
	public void componentResized(ComponentEvent e) {
		//Image temp = image.getScaledInstance(getWidth(), getHeight(), Image.SCALE_DEFAULT); // Dzia�a image = createImage(width, height);
		image = createImage(getWidth(), getHeight());
		buffer = (Graphics2D) image.getGraphics();
		buffer.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		device = (Graphics2D) getGraphics();
		device.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		for(int i = 0; i < numer; i++)
		{
			fig.get(i).UpdateBuffer(buffer);
			fig.get(i).UpdateDimension(getWidth(), getHeight());
		}
    }

    public void componentHidden(ComponentEvent e) {}
    public void componentMoved(ComponentEvent e) {}
    public void componentShown(ComponentEvent e) {}
}

package figury;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Rectangle2D;
import java.util.Random;


public class Trojkat extends Figura{

	public Trojkat(Graphics2D buf, int del, int w, int h) {
		super(buf, del, w, h);    
		Random generator = new Random();
		int X[] = {0, generator.nextInt(40), generator.nextInt(40)};
        int Y[] = {0, generator.nextInt(40)*-1, 0};
        shape = new Polygon(X, Y, 3);
		aft = new AffineTransform();                                  
		area = new Area(shape);
	}
}